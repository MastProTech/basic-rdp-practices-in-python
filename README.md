This repository contains my practice files for <b>Parser in Python</b>.
<hr>
I've implemented very basic grammars <b>using Recursive Descent Parsing</b>.
<br><b>RDP_1.py</b> Grammar:
<br>&emsp;S -> int op S|int
<br><b>RDP_2.py</b> Grammar:
<br>&emsp;S  -> cAd|bd
<br>&emsp;A  -> ab|a
<br><b>RDP_3.py</b> Grammar:
<br>&emsp;E->TE'
<br>&emsp;E'->+TE'|-TE'|NULL
<br>&emsp;T->nT'
<br>&emsp;T'->*nT'|/nT'|NULL