'''
Grammar:
E  -> TE'
E' -> +TE'|-TE'|NULL
T  -> nT'
T  -> *nT'|/nT'|NULL
'''

import re
# ----------------------------------- Using Regex to detect int
def is_int(word):
	patternNum=re.compile('^[0-9]+$')
	matchNum=patternNum.search(word)
	if matchNum is None:
		return False
	return True
# ----------------------------------- str(input)-> list(output)
def split_to_list(l):  
    l=l.replace('+',' + ')
    l=l.replace('-',' - ')
    l=l.replace('*',' * ')
    l=l.replace('/',' / ')
    l=l.replace('n',' n ')
    l=l.split()
    for i in range(0,len(l)):
        if is_int(l[i]):
            l[i]='n'
    print(l)
    return l
# ----------------------------------- Implementation of T RDP
def T(l, loc):
    if l[loc]=='n':
        print('n')
        loc=loc+1
        if l[loc]=='$':
            print('$')
            return loc, True
        elif l[loc]=='*' or l[loc]=='/':
            print('*|/')
            loc=loc+1
            print('T Recursion')
            loc, res=T(l, loc)
            if res:
                return loc, True
            else:
                return loc, False
        else:
            return loc, True
    else:
        return loc, False
# ----------------------------------- Implementation of E RDP
def E(l, loc=0):
    loc, res=T(l, loc)
    if res:
        if l[loc]=='$':
            print('$')
            return loc, True
        if l[loc]=='+' or l[loc]=='-':
            print('+|-')
            loc=loc+1
            print('E Recursion')
            loc, res=E(l, loc)
            if res:
                return loc, True
            else:
                return loc, False
        else:
            return loc, False
    else:
        return loc, False
# ----------------------------------- Main Program

# Some of acceptable strings:
# s='n*n+n*n-n/n'
# s='n'
# s='n+n'
# s='12+323-23+23232*2323/232'
s='12*232/23'
# s='1'
print('Input string:',s)
s=split_to_list(s)
s.append('$')
print(s)
temp, res=E(s)
if res:
    print('Successful')
else:
    print('Syntax Error')