'''
Grammar:
S  -> int op S|int       where int is any integer, op is any operator
'''
import re

def isInteger(word):
	patternNum=re.compile('^[0-9]+$')
	matchNum=patternNum.search(word)
	if matchNum is None:
		return False
	return True

def isOp(word): # Returns True if 'word' is operator
    arith=['+', '-', '*', '/', '%']
    if word in arith:
        return True
    return False

def rdp(l): # Applies RDP based on grammar written on the top of this file
    if isInteger(l[len(l)-1]):
        l.pop()
        print('First\n',l)
        print('Length',len(l))
        if len(l)>0:
            if isOp(l[len(l)-1]):
                l.pop()
                print('Second\n',l)
                print('Length',len(l))
                result=rdp(l)
                if result:
                    return True
                return False
        else:
            return True
    else:
        return False

l=['1']
print(l)
l.reverse()
print(l)
result=rdp(l)
if result:
    print("No Syntax Error")
else:
    print("Syntax Error")