'''
Grammar:
S  -> cAd|bd
A  -> ab|a
'''
def A(l, loc):
    if l[loc]=='a':
        print("'a' matched")
        loc=loc+1
        if l[loc]=='b':
            print("'b' matched")
            loc=loc+1
            return loc, True
        return loc, True
    return loc, False
def S(l, loc=0):
    if l[loc]=='c':
        print("'c' matched")
        loc=loc+1
        loc, res=A(l,loc)
        if res:
            if l[loc]=='d':
                print("'d' matched")
                loc=loc+1
                if l[loc]=='$':
                    print('All things true')
                    return True
                return False
            return False
        return False
    elif l[loc]=='b':
        print("'b' matched")
        loc=loc+1
        if l[loc]=='d':
            print("'d' matched")
            loc=loc+1
            if l[loc]=='$':
                print('All things true')
                return True
            return False
        return False
    else:
        return False


t='cabd'
t_char=[i for i in t]
t_char.append('$')
print(t_char)
if S(t_char):
    print('Successful')
else:
    print('Syntax Error')




# import re
# grammar=[
#     'S->AB',
#     'A->a',
#     'B->b'
# ]
# terminals=list()
# non_terminals=list()
# for word in grammar:
#     p_t='[A-Z]'
#     match_t=re.match(p_t, word)
#     terminals.append(match_t.group(0))
#     p_nt='[a-z]'
#     match_nt=re.finditer(p_nt, word)
#     for match in match_nt:
#         non_terminals.append(match.group(0))
# print(terminals)
# print(non_terminals)